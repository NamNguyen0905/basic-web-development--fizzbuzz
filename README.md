**Module 1: Basic Web Development**

A front end web application that implements the classic FizzBuzz programming challenge.

The main functionality of the application is essentially to implement the FizzBuzz challenge, with one caveat: instead of looping to 100, increment the current value to display via a button on the webpage. The user's score will be displayed on the page, and will be updated after each click to display the user's current score. The score readout must follow FizzBuzz formatting rules.
Additionally, using the provided back end API to store users' scores on a server.