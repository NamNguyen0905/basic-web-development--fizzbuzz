const express = require("express"),
  fs = require("fs"),
  util = require("util"),
  app = express(),
  bodyParser = require("body-parser");

app.set("port", process.env.PORT || 3000);

app.use(bodyParser.json());
app.use("/", express.static(__dirname));

let buffer = fs.readFileSync(__dirname + "/data");
var users = JSON.parse(buffer);

app.get("/users", (req, res) => {
  res.send("Server up. Please provide a user id to retrieve user scores.");
});

app.post("/users/:id", (req, res) => {
  let user = users.find(u => u.id === req.params.id);
  //Create or update user
  if (!user) {
    user = {
      id: req.params.id,
      score: 0
    };

    users.push(user);
    res.status(201);
  } else {
    user.score = req.body.score;
    if (user.score < 0 || !user.score || !req.is("application/json"))
      return res.status(400);
    else res.status(200);
  }

  fs.writeFileSync(
    __dirname + "/data",
    JSON.stringify(users, null, 1),
    "utf-8"
  );
  //Return the updated user
  res.send(user);
});

app.get("/users/:id", (req, res) => {
  const user = users.find(u => u.id === req.params.id);
  if (!user) {
    return res.status(404).send("User does not exist.");
  } else {
    res.status(200);
  }

  res.send(user);
});

app.listen(app.get("port"), function() {
  console.log("Server started: http://localhost:" + app.get("port") + "/");
});
