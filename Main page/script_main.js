function get(url) {
  return new Promise((resolve, reject) => {
    const http = new XMLHttpRequest();
    http.onload = function() {
      resolve({ status: http.status, data: JSON.parse(http.response) });
    };
    http.open("GET", url);
    http.send();
  });
}

function post(url, data) {
  data = JSON.stringify(data);
  return new Promise((resolve, reject) => {
    const http = new XMLHttpRequest();
    http.onload = function() {
      resolve({ status: http.status, data: JSON.parse(http.response) });
    };
    http.open("POST", url);
    //Make sure that the server knows we're sending it json data.
    http.setRequestHeader("Content-Type", "application/json");
    http.send(data);
  });
}

function FizzBuzz(score) {
  let el = document.getElementById("box");
  if (score != 0) {
    if (score % 3 == 0)
      if (score % 5 == 0) el.innerHTML = "FizzBuzz";
      else el.innerHTML = "Fizz";
    else if (score % 5 == 0) el.innerHTML = "Buzz";
    else el.innerHTML = score;
  } else el.innerHTML = score;
}

let user = localStorage.getItem("username");
let score;
// get("http://localhost/basic-web-development--fizzbuzz/api.php?id=" + user).then(
//   function(response) {
//     //Put all code that relies on the data from this request in here.

//     if (response.status == 200) {
//       const username = response.data.id; //The username that was requested. In this case it is "myUserName".
//       score = response.data.score; //The user's current score.
//     } else {
//       //User "myUserName" not found.
//       //response.data is null
//       score = 0;
//       post(
//         "http://localhost/basic-web-development--fizzbuzz/api.php?id=" + user,
//         { score: 0 }
//       ); //create a new user.
//     }
//     FizzBuzz(score);
//   }
// );

$(function() {
  $("p").prepend("<h1>Welcome, " + user + "</h1>");

  $.ajax({
    url: "/users/" + user,
    type: "GET",
    datatype: "application/json",
    success: data => {
      score = data["score"];
      FizzBuzz(score);
    },
    error: () => {
      score = 0;
      $.ajax({
        url: "/users/" + user,
        type: "POST",
        datatype: "application/json"
      });
      FizzBuzz(score);
    }
  });

  $('input[type="button"]').on("click", function() {
    score++;
    FizzBuzz(score);

    const dataToSend = { score: score };

    $.ajax({
      url: "/users/" + user,
      type: "POST",
      data: JSON.stringify(dataToSend),
      contentType: "application/json; charset=utf-8"
    });

    // post(
    //   "http://localhost/basic-web-development--fizzbuzz/api.php?id=" + user,
    //   dataToSend
    // ).then(function(response) {
    //   switch (response.status) {
    //     case 200:
    //       //User was updated successfully.
    //       //response.data will be the same as returned by get(), and should contain the updated data.
    //       score = response.data.score;
    //       break;
    //     case 201:
    //       //A new user was successfully created. Otherwise same as status 200.
    //       score = response.data.score;
    //       break;
    //     case 400:
    //       //Bad request. Most likely your data that you sent (in this case dataToSend) was formatted incorrectly, or you supplied a negative score value.
    //       //response.data will be: { Error: "error message" }
    //       console.error(response.data);
    //       break;
    //     case 500:
    //       //Something went wrong on the server, such as the database got deleted somehow. This should never happen.
    //       //response.data will be the same as status 400.
    //       console.error(response.data);
    //       break;
    //   }
    // });
  });
});
