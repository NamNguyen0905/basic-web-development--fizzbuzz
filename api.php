<?php

$userId           = isset($_REQUEST['id']) ? $_REQUEST['id'] : false;
$respond['error'] = false;
$respond['msg']   = null;

if (!$userId) {
    $respond['error'] = true;
    $respond['msg']   = 'Server up. Please provide a user id to retrieve user scores.';

    header('Content-Type: application/json');
    echo json_encode($respond);

    exit;
}

$filePath = __DIR__ . '/' . $userId . '.txt';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!file_exists($filePath)) {
        http_response_code(404);
        $respond['error'] = true;
        $respond['msg']   = 'User does not exist.';

        header('Content-Type: application/json');
        echo json_encode($respond);

        exit;
    }
    $respond['data'] = [
        'id'    => $userId,
        'score' => file_get_contents($filePath, true),
    ];

    header('Content-Type: application/json');
    echo json_encode($respond);

    exit;
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $data = json_decode(file_get_contents('php://input'), true);

    if (!isset($data['score'])) {
        $respond['error'] = true;
        $respond['msg']   = 'Score is required';
        http_response_code(400);
    } else {
        $filePath = __DIR__ . '/' . $userId . '.txt';

        if (!file_exists($filePath)) {
            http_response_code(201);
        }

        file_put_contents($filePath, $data['score']);

        $respond['data'] = [
            'id'    => $userId,
            'score' => $data['score'],
        ];
    }
}

